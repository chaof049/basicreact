/* 
day 1
- must return only one wrapper
- always add image in public
- in img tag . means public folder
- block element -> always starts from new line and occupies the whole space
- inline element -> occupies space as required only
- br tag does not support any style
- in react can store html element inside variable
- anything written inside return are displayed in browser
- call variable and js operation implementation inside tag using curly braces {}
day 2
- difference between function and component
- component is a function whose first letter must be capital and generally it returns html tag
- call function using parentheses but call component as a html tag
- component is a custom tag
- custom tag does not support any props like style, className, href
- props = passing value in component
- if props is other than string use curly braces {}
day 3
- cannot use if, else, for, while, do-while inside tag but can use array loop
- variable cannot be defined inside curly braces {}
- {} returns only one thing
- conditional(ternary) operator must have else part
- boolean are not displayed in browser, need to insert logic to display
- object cannot be used as a react child;(tag)
- in array wrapper are not displayed and element are placed one by one without coma(,)
- when setName , setAge is called, the page is rendered
day 4
-in onClick do non call function
day 6
- when normal variable is changed the page will not be rendered ;
- but when useState variable is changed the page will be rendered
- when page gets rendered ;
- if any state variable is updated then the component will be re-executed ;
- such that the state variable which is updated will hold the updated value ;
- and the other state variable holds the previous value ;
- when page gets rendered first all the content of components gets removed
- useEffect function is an asynchronous function ;
- it always executes at last (after printing the content in the browser)
day 7
- at first render useEffect function executes 
but from second render useEffect function depends on its dependencies
if one of the dependency changes, useEffect function will execute
- cleanUp function is a function returned by useEffect Function
it does not execute in first render
from second render, clean up function executes
- when useEffect function gets executed first clean up function executes
then the code above return will execute
- first render = component did mount
- component did unmount(component remove) 
- when component is removed(during hide and show) nothing gets executed 
but clean up function will execute
day 8
- while selecting component, it will see the more specific route
day 9 
- other        => value   ,  e.target.value
- checkbox     => checked ,  e.target.checked
- radioButton  => checked ,  e.target.value

e.preventDefault() => by default when the form is submitted, the page gets refreshed. 
so, to prevent this behavior , we have to use this function
*/
