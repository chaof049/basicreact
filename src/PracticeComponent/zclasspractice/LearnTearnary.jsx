import React from 'react'

const LearnTearnary = () => {
    let age=19
    let marks =60
    let calc =()=>{
        if (age<18)
        return <div>underage</div>
        else if (age>=18&&age<60)
        return <div>adult</div>
        else
        return <div>old</div>
    }
  return (
    <div>
        {
            age<18?<div>underage</div>
            :age>=18&&age<60?<div>adult</div>
            :<div>old</div>
        }
        {
            marks>39?<div>fail</div>
            :marks>=40&&marks<60?<div>third division</div>
            :marks>=60&&marks<80?<div>second division</div>
            :<div>first division</div>
        }
        {calc()}
    </div>
  )
}

export default LearnTearnary