import React from 'react'

const Location = (props) => {
  return (
    <div>
        country = {props.country}
        <br></br>
        province = {props.province}
        <br></br>
        district = {props.district}
        <br></br>
        exactLocation = {props.exactLocation}
    </div>
  )
}

export default Location