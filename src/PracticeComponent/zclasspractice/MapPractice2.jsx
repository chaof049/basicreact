import React from "react";
import { products } from "../../productData";

const MapPractice2 = () => {
  //   let task1 = ()=>{
  //     let desiredOutput = products.map((item,i)=>{
  //         return <p>{item.title}</p>
  //     })
  //     return desiredOutput
  //   }
  let task2 = () => {
    let op = products.map((item, i) => {
      return (
        <p>
          {item.title} costs NRS. {item.price} and its category is{" "}
          {item.category}
        </p>
      );
    });
    return op;
  };
  // let task3 =()=>{
  //     let desiredOutput = products
  //     .filter((item,i)=>{
  //         if (item.price>2000)
  //         return true
  //     })
  //     .map((item,i)=>{
  //         return <p>{item.title} costs NRS. {item.price} and its category is {item.category}</p>
  //     })
  //     return desiredOutput
  // }
  // let task4 =()=>{
  //     let desiredOutput = products.filter((item,i)=>{
  //         if (item.category==="Books")
  //         return true
  //     })
  //     .map((item,i)=>{
  //         return <p>{item.title} costs NRS. {item.price} and its category is {item.category}</p>
  //     })
  //     return desiredOutput
  // }
  let task5 = () => {
    let desiredOutput = products
      .map((item, i) => {
        return item.price;
      })
      .reduce((pre, cur) => {
        return pre + cur;
      }, 0);
    return desiredOutput;
  };

  return (
    <div>
      <h1>the products in our shop are</h1>
      {/* {task1()} */}
      {task2()}
      {/* {task3()} */}
      {/* {task4()} */}
      <p>the total cost is NRs.{task5()}</p>
    </div>
  );
};

export default MapPractice2;
