import React from 'react'

const ButtonClick = () => {
  let handleClick = (e)=>{
    console.log("button is clicked")
  }
  return (
    <div>
        {/* <button
        onClick={(e)=>{
          console.log("button is clicked")
          }}
          >
          Click me
        </button> */}
        <button
        onClick={handleClick}
        > Click me
        </button>
    </div>
  )
}

export default ButtonClick