import React, { useRef } from "react";

const UseRef1 = () => {
  let ref1 = useRef();
  let ref2 = useRef();
  let ref3 = useRef();
  return (
    <div>
      <button
        onClick={() => {
          ref3.current.style.backgroundColor = "pink";
          ref3.current.focus();
        }}
      >
        color 3
      </button>
      <button
        onClick={() => {
          ref3.current.focus();
        }}
      >
        focus 3
      </button>
      <button
        onClick={() => {
          ref3.current.blur();
        }}
      >
        blur 3
      </button>
      <input ref={ref1}></input>
      <input ref={ref2}></input>
      <input ref={ref3}></input>
    </div>
  );
};

export default UseRef1;
