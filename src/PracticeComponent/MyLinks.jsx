import React, { useState } from "react";
import { NavLink } from "react-router-dom";

const MyLinks = () => {
  let [token, setToken] = useState(localStorage.getItem("token"));
  return (
    <div>
      {/* <NavLink to="http://localhost:3000/products" className="mr-2">
        Product
      </NavLink>
      <NavLink to="http://localhost:3000/products/create" className="mr-2">
        Create Product
      </NavLink> */}
      {/* <NavLink
        to="http://localhost:3000/students"
        style={{ marginRight: "20px" }}
      >
        Student
      </NavLink>
      <NavLink
        to="http://localhost:3000/students/create"
        style={{ marginRight: "20px" }}
      >
        Create Student
      </NavLink> */}
      <NavLink to="http://localhost:3000/admin/register" className="mr-2">
        Register
      </NavLink>
      <NavLink to="http://localhost:3000/admin/login" className="mr-2">
        Login
      </NavLink>
      <NavLink
        to="http://localhost:3000/admin/logout"
        className="mr-2"
        onClick={() => {
          window.location.reload(true);
        }}
      >
        LogOut
      </NavLink>
      {token ? (
        <NavLink to="http://localhost:3000/admin/my-profile" className="mr-2">
          My Profile
        </NavLink>
      ) : null}

      <NavLink
        to="http://localhost:3000/admin/my-profile/update"
        className="mr-2"
      >
        Update
      </NavLink>
      <NavLink
        to="http://localhost:3000/admin/update-password"
        className="mr-2"
      >
        Update Password
      </NavLink>
      <NavLink to="http://localhost:3000/admin/reset-password" className="mr-2">
        Reset Password
      </NavLink>
      <NavLink
        to="http://localhost:3000/admin/forgot-password"
        className="mr-2"
      >
        Forgot Password
      </NavLink>
      <NavLink to="http://localhost:3000/admin/read-all-users" className="mr-2">
        Read All
      </NavLink>
      <NavLink
        to="http://localhost:3000/admin/read-specific-user"
        className="mr-2"
      >
        Read Specific
      </NavLink>
    </div>
  );
};

export default MyLinks;
