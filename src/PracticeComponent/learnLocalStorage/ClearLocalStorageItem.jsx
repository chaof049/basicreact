import React from "react";

const ClearLocalStorageItem = () => {
  //   localStorage.removeItem(`token`);
  return (
    <div>
      <button onClick={localStorage.removeItem(`token`)}>Clear</button>
    </div>
  );
};

export default ClearLocalStorageItem;
