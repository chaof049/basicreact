import axios from "axios";
import React from "react";

const UploadImage = ({ profileImage, setProfileImage }) => {

  let handleFile = async (e) => {
    // console.log("file");
    const formData = new FormData();
    // console.log(e.target.files);
    let images = [...e.target.files];

    images.forEach((file, index) => {
      formData.append("files", file);
    });

    // console.log(formData);

    try {
      let result = await axios({
        url: "http://localhost:8000/files",
        method: "POST",
        data: formData,
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });

      console.log(result.data.result[0]);

      setProfileImage(result.data.result[0]);
      // setFieldValue("file", result.data.result);
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <div>
      <form>
        <label>Files</label>
        <input type="file" name="files" multiple onChange={handleFile}></input>
        {/* {profileImage ? <img src={profileImage}></img> : null} */}
      </form>
    </div>
  );
};

export default UploadImage;
