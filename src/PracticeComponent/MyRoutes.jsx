import React from "react";
import {Route, Routes } from "react-router-dom";
import ProductForm from "./product/ProductForm";
import ReadAllProduct from "./product/ReadAllProduct";
import StudentForm from "./student/StudentForm";
import ReadAllStudent from "./student/ReadAllStudent";
import ReadSpecificProduct from "./product/ReadSpecificProduct";
import ReadSpecificStudent from "./student/ReadSpecificStudent";

const MyRoutes = () => {
  return (
    <div>
      <Routes>
        <Route path="/"></Route>
        <Route
          path="/products/create"
          element={<ProductForm></ProductForm>}
        ></Route>
        <Route
          path="/products"
          element={<ReadAllProduct></ReadAllProduct>}
        ></Route>
        <Route
          path="/products/:id"
          element={<ReadSpecificProduct></ReadSpecificProduct>}
        ></Route>
        <Route
          path="/students/create"
          element={<StudentForm></StudentForm>}
        ></Route>
        <Route
          path="/students"
          element={<ReadAllStudent></ReadAllStudent>}
        ></Route>
        <Route
          path="/students/:id"
          element={<ReadSpecificStudent></ReadSpecificStudent>}
        ></Route>
        <Route path="*" element={<div>404 page</div>}></Route> 
        {/* it will be called if route is other than defined route */}
      </Routes>
    </div>
  );
};

export default MyRoutes;
