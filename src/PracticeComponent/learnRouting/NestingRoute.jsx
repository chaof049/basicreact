import React from "react";
import { Outlet, Route, Routes } from "react-router-dom";

const NestingRoute = () => {
  return (
    <div>
      <Routes>
        {/* <Route
          path="/"
          element={
            <div>
              <Outlet></Outlet>
            </div>
          }
        >
            <Route index element={<div>Home Page</div>}></Route>
            <Route path="student" element={<div>Student Page</div>}></Route>
            <Route path="*" element={<div>404 Error Page</div>}></Route>
        </Route> */}
        <Route
          path="/"
          element={
            <div>
              <Outlet></Outlet>
            </div>
          }
        >
          <Route index element={<div>Home Page</div>}></Route>
          <Route path="create" element={<div>Create blog</div>}></Route>
          <Route path=":id" element={<div>Detail blog</div>}></Route>
          <Route
            path="update"
            element={
              <div>
                Update<Outlet></Outlet>
              </div>
            }
          >
            <Route path=":id" element={<div>Update blog</div>}></Route>
          </Route>
        </Route>
      </Routes>
    </div>
  );
};

export default NestingRoute;
