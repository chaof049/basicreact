import React from "react";
import { Outlet, Route, Routes } from "react-router-dom";
import ReadAllProduct from "../product/ReadAllProduct";
import ReadSpecificProduct from "../product/ReadSpecificProduct";
import ProductForm from "../product/ProductForm";
import UpdateProduct from "../product/UpdateProduct";
import ReadAllStudent from "../student/ReadAllStudent";
import ReadSpecificStudent from "../student/ReadSpecificStudent";
import StudentForm from "../student/StudentForm";
import UpdateStudent from "../student/UpdateStudent";
import MyLinks from "../MyLinks";
import Register from "../userManagement/Register";
import Login from "../userManagement/Login";
import Verify from "../userManagement/Verify";
import MyProfile from "../userManagement/MyProfile";
import Logout from "../userManagement/Logout";
import UpdateProfile from "../userManagement/UpdateProfile";
import UpdatePassword from "../userManagement/UpdatePassword";
import ForgotPassword from "../userManagement/ForgotPassword";
import ReadSpecificUser from "../userManagement/ReadSpecificUser";
import ReadUsers from "../userManagement/ReadUsers";
import ResetPassword from "../userManagement/ResetPassword";

const ReactRoutes = () => {
  return (
    <div>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <MyLinks></MyLinks>
              <Outlet></Outlet>
              {/* <div>This is footer</div> */}
            </div>
          }
        >
          <Route index element={<div> this is home page</div>}></Route>
          {/* <Route
            path="products"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<ReadAllProduct></ReadAllProduct>}></Route>
            <Route
              path=":id"
              element={<ReadSpecificProduct></ReadSpecificProduct>}
            ></Route>
            <Route path="create" element={<ProductForm></ProductForm>}></Route>
            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route
                path=":id"
                element={<UpdateProduct></UpdateProduct>}
              ></Route>
            </Route>
          </Route> */}

          {/* <Route
            path="students"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<ReadAllStudent></ReadAllStudent>}></Route>
            <Route
              path=":id"
              element={<ReadSpecificStudent></ReadSpecificStudent>}
            ></Route>
            <Route path="create" element={<StudentForm></StudentForm>}></Route>
            <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route
                path=":id"
                element={<UpdateStudent></UpdateStudent>}
              ></Route>
            </Route>
          </Route> */}
          <Route path="verify-email" element={<Verify></Verify>}></Route>
          <Route
            path="reset-password"
            element={<ResetPassword></ResetPassword>}
          ></Route>
          <Route path="admin" element={<Outlet></Outlet>}>
            <Route index element={<div>Admin page</div>}></Route>
            <Route path="register" element={<Register></Register>}></Route>
            <Route path="login" element={<Login></Login>}></Route>
            <Route
              path="update-password"
              element={<UpdatePassword></UpdatePassword>}
            ></Route>
            <Route
              path="forgot-password"
              element={<ForgotPassword></ForgotPassword>}
            ></Route>
            <Route
              path="read-specific-user"
              element={<ReadSpecificUser></ReadSpecificUser>}
            ></Route>
            <Route
              path="read-all-users"
              element={<ReadUsers></ReadUsers>}
            ></Route>
            <Route
              path="my-profile"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route index element={<MyProfile></MyProfile>}></Route>
              <Route
                path="update"
                element={<UpdateProfile></UpdateProfile>}
              ></Route>
            </Route>
            <Route path="logout" element={<Logout></Logout>}></Route>
          </Route>
        </Route>

        <Route path="*" element={<div>404 page</div>}></Route>
      </Routes>
    </div>
  );
};

export default ReactRoutes;

// /   => this is home page
// /products => read all products
// /products/:id => detail page
// /products/create => create products
// /products/update/:id => products update
