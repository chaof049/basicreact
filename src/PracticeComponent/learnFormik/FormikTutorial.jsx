import { Form, Formik } from "formik";
import React from "react";
import * as yup from "yup";
import FormikInput from "./FormikInput";
import FormikRadio from "./FormikRadio";
import FormikSelect from "./FormikSelect";
import FormikCheckBox from "./FormikCheckBox";
import FormikTextArea from "./FormikTextArea";

const FormikTutorial = () => {
  let initialValues = {
    fullName: "",
    email: "",
    password: "",
    gender: "",
    country: "",
    isMarried: false,
    phoneNumber: 0,
    age: 0,
    description: "",
  };
  let onSubmit = (value, other) => {
    console.log(value);
  };
  let validationSchema = yup.object({
    fullName: yup
      .string()
      .required("Full Name is required")
      .min(5, "must be at least 5 characters")
      .max(15, "must be at least 15 characters")
      .matches(/^[a-zA-Z ]+$/, "only alphabet and space are allowed"),
    email: yup
      .string()
      .required("Email is required")
      .matches(
        /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,}$/,
        "must be a valid email"
      ),
    password: yup
      .string()
      .required("Password is required")
      .matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*d)(?=.*[!@#$%^&*()_+])[A-Za-zd!@#$%^&*()_+]{8,}$/,
        "must contain at least one uppercase, lowercase, number, a symbol and 8 character long"
      ),
    gender: yup.string().required("Gender is required"),
    country: yup.string().required("Country is required"),
    isMarried: yup.boolean(),
    phoneNumber: yup
      .number()
      .required("phoneNumber is required")
      .min(10, "must be at least 10 character")
      .max(10, "must be at most 10 character"),
    age: yup
      .number()
      .required("Age is required")
      .min(18, "must be 18 or above"),
    description: yup.string(),
  });
  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];
  let countryOptions = [
    { label: "Select Country", value: "", disabled: true },
    { label: "Nepal", value: "nepal" },
    { label: "China", value: "china" },
    { label: "Brazil", value: "brazil" },
  ];
  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {(formik) => {
          return (
            <Form>
              <FormikInput
                name="fullName"
                label="Full Name"
                type="text"
                // onChange={(e) => {
                //   formik.setFieldValue("fullName", e.target.value);
                // }}
                required={true}
              ></FormikInput>
              <FormikInput
                name="email"
                label="Email"
                type="email"
                // onChange={(e) => {
                //   formik.setFieldValue("email", e.target.value);
                // }}
                required={true}
              ></FormikInput>
              <FormikInput
                name="password"
                label="Password"
                type="password"
                // onChange={(e) => {
                //   formik.setFieldValue("password", e.target.value);
                // }}
                required={true}
              ></FormikInput>
              <FormikRadio
                name="gender"
                label="Gender"
                // onChange={(e) => {
                //   formik.setFieldValue("gender", e.target.value);
                // }}
                required={true}
                options={genders}
              ></FormikRadio>
              <FormikSelect
                name="country"
                label="Country"
                // onChange={(e) => {
                //   formik.setFieldValue("country", e.target.value);
                // }}
                required={true}
                options={countryOptions}
              ></FormikSelect>
              <FormikCheckBox
                name="isMarried"
                label="Is Married"
                // onChange={(e) => {
                //   formik.setFieldValue("isMarried", e.target.checked);
                // }}
              ></FormikCheckBox>
              <FormikInput
                name="phoneNumber"
                label="Phone Number"
                type="text"
                // onChange={(e) => {
                //   formik.setFieldValue("phoneNumber", e.target.value);
                // }}
                required={true}
              ></FormikInput>
              <FormikInput
                name="age"
                label="Age"
                type="number"
                // onChange={(e) => {
                //   formik.setFieldValue("age", e.target.value);
                // }}
                required={true}
              ></FormikInput>
              <FormikTextArea
                name="description"
                label="Description"
                // onChange={(e) => {
                //   formik.setFieldValue("description", e.target.value);
                // }}
              ></FormikTextArea>
              <button type="submit">Submit</button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default FormikTutorial;
