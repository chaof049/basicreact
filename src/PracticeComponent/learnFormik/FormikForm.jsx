import { Field, Form, Formik } from "formik";
import React from "react";
import * as yup from "yup";
import FormikInput from "./FormikInput";
import FormikTextArea from "./FormikTextArea";
import FormikSelect from "./FormikSelect";
import FormikRadio from "./FormikRadio";
import FormikCheckBox from "./FormikCheckBox";

const FormikForm = () => {
  let initialValues = {
    firstName: "",
    description: "",
    days: "",
    gender: "",
    isMarried: false,
  };
  let onSubmit = (value, other) => {
    console.log(value);
  };
  let validationSchema = yup.object({
    firstName: yup.string().required("fist name is required"),
    description: yup.string().required("description is required"),
  });
  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];
  let days = [
    {
      label: "segunda-feira",
      value: "day1",
    },
    {
      label: "terça-feira ",
      value: "day2",
    },
    {
      label: "quarta-feira",
      value: "day3",
    },
    {
      label: "quinta-feira",
      value: "day4",
    },
    {
      label: "sexta-feira",
      value: "day5",
    },
    {
      label: "sábado",
      value: "day6",
    },
    {
      label: "domingo",
      value: "day7",
    },
  ];
  return (
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        {(formik) => {
          return (
            <Form>
              <FormikInput
                name="firstName"
                label="First Name: "
                type="text"
                required={true}
              ></FormikInput>
              <FormikTextArea
                name="description"
                label="Description: "
                type="text"
                required={true}
              ></FormikTextArea>
              <FormikSelect
                name="days"
                label="Days: "
                required={true}
                options={days}
              ></FormikSelect>
              <FormikRadio
                name="gender"
                label="Gender: "
                required={true}
                options={genders}
              ></FormikRadio>
              <FormikCheckBox
                name="isMarried"
                label="Is Married: "
              ></FormikCheckBox>

              <button type="submit">Submit</button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default FormikForm;
