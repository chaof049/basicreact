import axios from "axios";
import { Field } from "formik";
import React from "react";

const FormikUpload = ({
  name,
  label,
  onChange,
  required,
  setFieldValue,
  ...props
}) => {


  return (
    <div>
      <Field name={name}>
        {({ field, form, meta }) => {

          console.log(form)
          return (
            <div>
              <label htmlFor={name}>
                {label}{" "}
                {required ? <span style={{ color: "red" }}>*</span> : null}
              </label>
              <input
                {...field}
                {...props}
                type="file"
                id={name}
                value={meta.value}
                multiple
                onChange={onChange}
                // onChange={onChange}
              ></input>
              {/* {meta.touched && meta.error ? (
                <div style={{ color: "red" }}>{meta.error}</div>
              ) : null}
              {meta.value ? (
                <img src={meta.value} alt="profileImage"></img>
              ) : null} */}
            </div>
          );
        }}
      </Field>
    </div>
  );
};

export default FormikUpload;
