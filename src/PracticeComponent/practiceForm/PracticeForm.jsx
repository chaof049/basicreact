import React, { useState } from "react";
import axios from "axios";
import { Form, Formik } from "formik";
import FormikInput from "../learnFormik/FormikInput";
import FormikUpload from "../learnFormik/FormikUpload";

const PracticeForm = () => {
  let initialValues = {
    name: "",
    profileImage: "",
  };
  let onSubmit = async (values, other) => {
    console.log(values);
    try {
      let result = await axios({
        url: `http://localhost:8000/forms`,
        method: "POST",
        data: values,
      });
      console.log(result);
    } catch (error) {
      console.log(error.response.data.message);
    }
  };

  return (
    <div>
      <Formik initialValues={initialValues} onSubmit={onSubmit}>
        {({ setFieldValue }) => {
          return (
            <Form>
              <FormikInput
                name="name"
                label="Name: "
                type="text"
                required={true}
              ></FormikInput>
              {/* <UploadImage setFieldValue={setFieldValue}></UploadImage> */}
              <FormikUpload
                name="profileImage"
                label="File: "
                // onChange={(e) => {
                //   // setFieldValue("profileImage", e.target.files[0]);
                //   setFieldValue={setFieldValue}
                // }}
                setFieldValue={setFieldValue}
              ></FormikUpload>
              <button type="submit">Submit</button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default PracticeForm;
