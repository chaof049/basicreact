import React, { useState } from "react";
import axios from "axios";
import UploadImage from "../Upload/UploadImage";

const PracticeForm2 = () => {
  let [name, setName] = useState("");
  let [profileImage, setProfileImage] = useState("");

  let onSubmit = async (e) => {
    e.preventDefault();

    let data = {
      name: name,
      profileImage: profileImage,
    };
    console.log(data);
    try {
      let result = await axios({
        url: `http://localhost:8000/forms`,
        method: "POST",
        data: data,
      });
      setName("");
      setProfileImage("");
    } catch (error) {
      console.log(error.message);
    }
  };
  return (
    <div>
      <form onSubmit={onSubmit}>
        <div>
          <div>
            <label htmlFor="name">Name : </label>
            <input
              type="text"
              id="name"
              placeholder="Eg: wangyi"
              value={name}
              onChange={(e) => {
                setName(e.target.value);
              }}
            ></input>
          </div>
          <br></br>
          {/* <div>
            <label htmlFor="profileImage">Profile Image : </label>
            <input
              type="file"
              id="profileImage"
              value={profileImage}
              onChange={(e) => {
                setProfileImage(e.target.value);
              }}
            ></input>
          </div> */}
          <UploadImage
            profileImage={profileImage}
            setProfileImage={setProfileImage}
          ></UploadImage>
          <br></br>
        </div>
        <button type="submit">Proceed</button>
      </form>
    </div>
  );
};

export default PracticeForm2;
