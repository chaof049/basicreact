import React, { useEffect, useState } from "react";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate, useParams } from "react-router-dom";

const UpdateProduct = () => {
  let [name, setName] = useState("");
  let [price, setPrice] = useState("");
  let [quantity, setQuantity] = useState("");

  let params = useParams()
  let navigate = useNavigate()

  let onSubmit = async (e) => {
    e.preventDefault();

    let data = {
      name: name,
      price: price,
      quantity: quantity,
    };
    // console.log(data);
    try {
      let result = await axios({
        url: `http://localhost:8000/products/${params.id}`,
        method: "PATCH",
        data: data,
      });
      navigate(`/products/${params.id}`)
    } catch (error) {
      toast.error(error.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };
  let getProduct = async () => {
    let result = await axios({
      url: `http://localhost:8000/products/${params.id}`,
      method: "GET",
    });
    let data = result.data.result
    setName(data.name)
    setPrice(data.price)
    setQuantity(data.quantity)
  };
  useEffect(() => {
    getProduct();
  }, []);
  return (
    <div>
      <ToastContainer />
      <form onSubmit={onSubmit}>
        <div>
          <div>
            <label htmlFor="productName">Product Name : </label>
            <input
              type="text"
              id="productName"
              placeholder="Eg: wangyi"
              value={name}
              onChange={(e) => {
                setName(e.target.value);
              }}
            ></input>
          </div>
          <br></br>
          <div>
            <label htmlFor="price">Price : </label>
            <input
              type="number"
              id="price"
              placeholder="999"
              value={price}
              onChange={(e) => {
                setPrice(e.target.value);
              }}
            ></input>
          </div>
          <br></br>
          <div>
            <label htmlFor="quantity">Quantity : </label>
            <input
              type="number"
              id="quantity"
              placeholder="Eg: 69"
              value={quantity}
              onChange={(e) => {
                setQuantity(e.target.value);
              }}
            ></input>
          </div>
          <br></br>
        </div>
        <button
          type="submit"
          className="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
        >
          Update
        </button>
      </form>
    </div>
  );
};

export default UpdateProduct;
