import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
const MySwal = withReactContent(Swal);

const ReadAllProduct = () => {
  let [products, setProducts] = useState([]);
  let navigate = useNavigate();

  let getAllProducts = async () => {
    try {
      let result = await axios({
        url: "http://localhost:8000/products",
        method: "GET",
      });
      setProducts(result.data.result);
    } catch (error) {
      console.log(error.response.data.message);
    }
  };

   let handleDelete = (id) => {
     return (e) => {
       MySwal.fire({
         title: "Confirmation",
         text: "Are you sure you want Delete?",
         showCancelButton: true,
         confirmButtonText: "Delete",
         cancelButtonText: "Cancel",
       }).then(async (result) => {
         if (result.isConfirmed) {
           try {
             let result = await axios({
               url: `http://localhost:8000/products/${id}`,
               method: "DELETE",
             });
             getAllProducts();
             toast.success(result.data.message);
           } catch (error) {
             toast.error(error.response.data.message);
           }
         } else if (result.isDismissed) {
           console.log("cancel button is clicked");
         }
       });
     };
   };
  let handleView = (id) => {
    return (e) => {
      navigate(`/products/${id}`);
    };
  };

  let handleUpdate = (id) => {
    return (e) => {
      navigate(`/products/update/${id}`);
    };
  };

  useEffect(() => {
    getAllProducts();
  }, []);
  return (
    <>
      <ToastContainer />
      {products.map((item, i) => {
        return (
          <div key={i} class="border-dashed border-2 border-indigo-600 mb-2">
            <p>Product is {item.name}</p>
            <p>Product price is {item.price}</p>
            <p>Product quantity is {item.quantity}</p>
            <button
              className="rounded bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 mr-4"
              onClick={handleUpdate(item._id)}
            >
              Edit
            </button>
            <button
              className="rounded bg-green-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 mr-4"
              onClick={handleView(item._id)}
            >
              View
            </button>
            <button
              className="rounded bg-red-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-red-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 mr-4"
              onClick={handleDelete(item._id)}
            >
              Delete
            </button>
          </div>
        );
      })}
    </>
  );
};

export default ReadAllProduct;
