import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const ReadSpecificProduct = () => {
  // let params = useParams();
  // let id = params.id;

  // let [query] = useSearchParams();
  // console.log(query.get("name"));
  // console.log(query.get("age"));

  // let navigate = useNavigate();

  let [product, setProduct] = useState({});

  let params = useParams();

  let getProduct = async () => {
    let result = await axios({
      url: `http://localhost:8000/products/${params.id}`,
      method: "GET",
    });
    setProduct(result.data.result);
  };
  useEffect(() => {
    getProduct();
  }, []);
  return (
    <div>
      <p>Product is {product.name}</p>
      <p>Product price is {product.price}</p>
      <p>Product quantity is {product.quantity}</p>
    </div>
  );
};

export default ReadSpecificProduct;
