import React, { useState } from "react";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const ProductForm = () => {
  let [name, setName] = useState("");
  let [price, setPrice] = useState("");
  let [quantity, setQuantity] = useState("");

  let onSubmit = async (e) => {
    e.preventDefault();

    let data = {
      name: name,
      price: price,
      quantity: quantity,
    };
    // console.log(data);
    try {
      let result = await axios({
        url: `http://localhost:8000/products`,
        method: "POST",
        data: data,
      });
      setName("");
      setPrice("");
      setQuantity("");
      toast.success(result.data.message, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    } catch (error) {
      toast.error(error.response.data.message, {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
        theme: "light",
      });
    }
  };
  return (
    <div>
      <ToastContainer />
      <form onSubmit={onSubmit}>
        <div>
          <div>
            <label htmlFor="productName">Product Name : </label>
            <input
              type="text"
              id="productName"
              placeholder="Eg: wangyi"
              value={name}
              onChange={(e) => {
                setName(e.target.value);
              }}
            ></input>
          </div>
          <br></br>
          <div>
            <label htmlFor="price">Price : </label>
            <input
              type="number"
              id="price"
              placeholder="999"
              value={price}
              onChange={(e) => {
                setPrice(e.target.value);
              }}
            ></input>
          </div>
          <br></br>
          <div>
            <label htmlFor="quantity">Quantity : </label>
            <input
              type="number"
              id="quantity"
              placeholder="Eg: 69"
              value={quantity}
              onChange={(e) => {
                setQuantity(e.target.value);
              }}
            ></input>
          </div>
          <br></br>
        </div>
        <button
          type="submit"className="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
          
        >
          Proceed
        </button>
      </form>
    </div>
  );
};

export default ProductForm;
