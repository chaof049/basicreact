import React, { useState } from 'react'

const ShowAndHideImage = () => {
    let [image, setShowImage]=useState(true)
    let handleImage =(isDisplay)=>{
        return (e)=>{
            setShowImage(isDisplay)
        }
    }
  return (
    <div>
        {
            image?<img src='./logo192.png' alt='my logo'></img>:null
        }
        <br></br>
        <button
        onClick={handleImage(true)}>Show</button>
        <br></br>
        <button
        onClick={handleImage(false)}>Hide</button>
    </div>
  )
}

export default ShowAndHideImage

// handleClick ; (e)=>{} ; it is used when value don't need to be passed
// handleClick() ; ()=>{return((e)=>{})} ; when a value is needed to be passed