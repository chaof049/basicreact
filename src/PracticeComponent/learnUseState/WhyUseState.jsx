import React, { useState } from 'react'

const WhyUseState = () => {
    let [name, setName]=useState("chaofan")
  return (
    <div>
        {name}
        <br></br>
        <button
        onClick={(e)=>{
            setName("xiaowei")
        }}>Click Me</button>
    </div>
  )
}

export default WhyUseState