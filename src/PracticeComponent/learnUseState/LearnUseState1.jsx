import React, { useState } from 'react'

const LearnUseState1 = () => {
    let [name, setName]= useState("chaofan")
    let [age, setAge]=useState(33)
    let [count, setCount]= useState(0)
    let handleCLick = (e)=>{
        setName("jackie chan")   
    }
    let handleAge = (e)=>{
        setAge(35)
    }
    let handleIncrement = (e)=>{
      setCount(count<10?count+1:10)
    }
    let handleDecrement = (e)=>{
      setCount(count>0?count-1:0)
    }
    let handleReset = (e)=>{
      setCount(0)
    }
  return (
    <div>my name is {name}
    <br></br>
    <button
    onClick={handleCLick}>Change Name</button>
    <br></br>
    my age is {age}
    <br></br>
    <button
    onClick={handleAge}>Change Age</button>
    <br></br>
    {count}
    <br></br>
    <button
    onClick={handleIncrement}>Add</button>
    <br></br>
    <button
    onClick={handleDecrement}>Subtract</button>
    <br></br>
    <button
    onClick={handleReset}>Reset</button>
    </div>
  )
}

export default LearnUseState1