import React, { useState } from 'react'

const ImageUseState = () => {
    let [image, setShowImage]=useState(true)
    let showImage = (e)=>{
        setShowImage(true)
    }
    let hideImage = (e)=>{
        setShowImage(false)
    }
    
    
  return (
    <div>
        {
            image?<img src='./logo192.png' alt='my logo'></img>:null
        }
        <br></br>
        <button
        onClick={showImage}>Show Image</button>
        <br></br>
        <button
        onClick={hideImage}>Hide Image</button>
        <br></br>
        
    </div>
  )
}

export default ImageUseState