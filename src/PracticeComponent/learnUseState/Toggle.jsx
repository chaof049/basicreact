import React, { useState } from 'react'

const Toggle = () => {
    let [showImg, setShowImg]= useState(false)
    let handleClick = ()=>{
        if(showImg){
                setShowImg(false)
            } else {
                setShowImg(true)
            }
    }
  return (
    <div>
        {
            showImg?<img src="./logo192.png" alt='logo'></img>:null
        }
        <button onClick={handleClick}>{showImg?"Hide":"Show"}</button>
    </div>
  )
}

export default Toggle