import React, { useState } from "react";

const Form = () => {
  let [name, setName] = useState("");
  let [lastName, setLastName] = useState("");
  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");
  let [date, setDate] = useState("");
  let [isMarried, setIsMarried] = useState(false);
  let [day, setDay] = useState("day1");
  let [gender, setGender] = useState("male");

  let onSubmit = (e) => {
    e.preventDefault();
    let data = {
      name: name,
      lastName: lastName,
      email: email,
      password: password,
      date: date,
      isMarried: isMarried,
      day: day,
      gender: gender,
    };
    console.log(data);
  };
  let days = [
    {
      label: "segunda-feira",
      value: "day1",
    },
    {
      label: "terça-feira ",
      value: "day2",
    },
    {
      label: "quarta-feira",
      value: "day3",
    },
    {
      label: "quinta-feira",
      value: "day4",
    },
    {
      label: "sexta-feira",
      value: "day5",
    },
    {
      label: "sábado",
      value: "day6",
    },
    {
      label: "domingo",
      value: "day7",
    },
  ];
  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];
  return (
    <div>
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="firstName">First Name : </label>
          <input
            type="text"
            id="firstName"
            placeholder="Eg: wangyi"
            value={name}
            onChange={(e) => {
              setName(e.target.value);
            }}
          ></input>
          <br></br>
          <label htmlFor="lastName">Last Name : </label>
          <input
            type="text"
            id="lastName"
            placeholder="Eg: wangyi"
            value={lastName}
            onChange={(e) => {
              setLastName(e.target.value);
            }}
          ></input>
          <br></br>
          <label htmlFor="email">Email : </label>
          <input
            type="email"
            id="email"
            placeholder="Eg: wangyi@yahoo.com"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          ></input>
          <br></br>
          <label htmlFor="password">Password : </label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          ></input>
          <br></br>
          <label htmlFor="date">Date Of Birth : </label>
          <input
            type="date"
            id="date"
            value={date}
            onChange={(e) => {
              setDate(e.target.value);
            }}
          ></input>
          <br></br>
          <label htmlFor="isMarried">Married : </label>
          <input
            type="checkbox"
            id="isMarried"
            checked={isMarried === true}
            onChange={(e) => {
              setIsMarried(e.target.checked);
            }}
          ></input>
          <br></br>
        </div>
        <div>
          <label htmlFor="day">Day : </label>
          <select
            value={day}
            id="day"
            onChange={(e) => {
              setDay(e.target.value);
            }}
          >
            {/* <option value="day1">segunda-feira</option>
            <option value="day2">terça-feira </option>
            <option value="day3">quarta-feira </option>
            <option value="day4">quinta-feira </option>
            <option value="day5">sexta-feira</option>
            <option value="day6">sábado</option>
            <option value="day7">domingo</option> */}
            {days.map((item, i) => {
              return <option value={item.value}>{item.label}</option>;
            })}
          </select>
        </div>
        <div>
          <label htmlFor="male">Gender : </label>

          {genders.map((item, i) => {
            return (
              <>
                <label htmlFor={item.value}>{item.label}</label>
                <input
                  type="radio"
                  value={item.value}
                  id={item.value}
                  checked={gender === item.value}
                  onChange={(e) => {
                    setGender(e.target.value);
                  }}
                ></input>
              </>
            );
          })}

          {/* <label htmlFor="male">Male</label>
          <input type="radio" value="male" id="male"
          checked={gender==='male'}
          onChange={(e)=>{
            setGender(e.target.value)
          }}
          ></input>
          <label htmlFor="female">Female</label>
          <input type="radio" value="female" id="female"
          checked={gender==='female'}
          onChange={(e)=>{
            setGender(e.target.value)
          }}
          ></input>
          <label htmlFor="others">Others</label>
          <input type="radio" value="others" id="others"
          checked={gender==='others'}
          onChange={(e)=>{
            setGender(e.target.value)
          }}
          ></input> */}
        </div>
        <button
          type="submit"
          className="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
        >
          Proceed
        </button>
      </form>
    </div>
  );
};

export default Form;
