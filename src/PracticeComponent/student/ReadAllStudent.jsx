import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const ReadAllStudent = () => {
  let [students, setStudents] = useState([]);
  let navigate = useNavigate();

  let getAllStudents = async () => {
    try {
      let result = await axios({
        url: "http://localhost:8000/students",
        method: "GET",
      });
      setStudents(result.data.result);
    } catch (error) {
      console.log(error.response.data.message);
    }
  };
  useEffect(() => {
    getAllStudents();
  }, []);
  return (
    <>
      {students.map((item, i) => {
        return (
          <div key={i} class="border-dashed border-2 border-indigo-600 mb-2">
            <p>Student name is {item.name}</p>
            <p>Student age is {item.age}</p>
            <p>Student is {item.isMarried}</p>
            <button className="rounded bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 mr-4">
              Edit
            </button>
            <button
              className="rounded bg-green-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 mr-4"
              onClick={() => {
                navigate(`/students/${item._id}`);
              }}
            >
              View
            </button>
            <button
              className="rounded bg-red-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-red-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 mr-4"
              onClick={async (e) => {
                try {
                  let result = await axios({
                    url: `http://localhost:8000/students/${item._id}`,
                    method: "DELETE",
                  });
                } catch (error) {
                  console.log(error.response.data.message);
                }
              }}
            >
              Delete
            </button>
          </div>
        );
      })}
    </>
  );
};

export default ReadAllStudent;
