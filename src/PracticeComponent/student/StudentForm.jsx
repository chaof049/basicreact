import React from "react";
import axios from "axios";
import { Form, Formik } from "formik";
import FormikInput from "../learnFormik/FormikInput";
import FormikCheckBox from "../learnFormik/FormikCheckBox";

const StudentForm = () => {
  let initialValues = {
    name: "",
    age: "",
    isMarried: false,
  };
  let onSubmit = async (value, other) => {
    console.log(value);
    // console.log(other)

    try {
      let result = await axios({
        url: `http://localhost:8000/students`,
        method: "POST",
        data: value,
      });
    } catch (error) {
      console.log(error.response.data.message);
    }
    other.restForm();
  };

  return (
    <div>
      <Formik initialValues={initialValues} onSubmit={onSubmit}>
        {(formik) => {
          return (
            <Form>
              <FormikInput
                name="name"
                label="Name: "
                type="text"
                required={true}
              ></FormikInput>
              <FormikInput
                name="age"
                label="Age: "
                type="number"
                required={true}
              ></FormikInput>
              <FormikCheckBox
                name="isMarried"
                label="Is Married: "
              ></FormikCheckBox>
              <button type="submit">Submit</button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default StudentForm;
