import axios from "axios";
import React, { useState } from "react";

const ForgotPassword = () => {
  let [email, setEmail] = useState("");

  let onSubmitForm = async (e) => {
    e.preventDefault();

    console.log("first");

    let data = {
      email: email,
    };

    let result = await axios({
      url: "http://localhost:8000/users/forgot-password",
      method: "POST",
      data: data,
    });
  };

  return (
    <div>
      <p>To find your account, please enter the email</p>
      <form onSubmit={onSubmitForm}>
        <div>
          <label htmlFor="email">Email: </label>
          <input
            type="email"
            // placeholder="1000"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
            id="email"
          ></input>
        </div>

        <button type="submit">Send Reset Link</button>
      </form>
    </div>
  );
};

export default ForgotPassword;
