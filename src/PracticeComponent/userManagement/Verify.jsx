import axios from "axios";
import React, { useEffect } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";

const Verify = () => {
  let [query] = useSearchParams();
  let token = query.get("token");

  let navigate = useNavigate();

  let verify = async () => {
    try {
      let result = await axios({
        url: "http://localhost:8000/users/verify-email",
        method: "PATCH",
        headers: {
          Authorization: `Bearer ${token}`,
        },
        
      });
      navigate(`/admin/login`);
    } catch (error) {
      console.log(error.message);
    }
  };

  useEffect(() => {
    verify();
  }, []);

  return <div></div>;
};

export default Verify;
