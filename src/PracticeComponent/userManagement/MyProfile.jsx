import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

const MyProfile = () => {
  let [myProfile, setMyProfile] = useState({});

  let navigate=useNavigate()

  let token = localStorage.getItem(`token`);

  let getMyProfile = async () => {
    let result = await axios({
      url: `http://localhost:8000/users/myprofile`,
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    // console.log(result)
    setMyProfile(result.data.result);
  };
  useEffect(() => {
    getMyProfile();
  }, []);

  return (
    <div>
      <p>Full Name is {myProfile.fullName}</p>
      <p>Email is {myProfile.email}</p>
      <p>Gender is {myProfile.gender}</p>
      <p>DOB is {new Date(myProfile.dob).toLocaleDateString()}</p>
      <br></br>
      <button onClick={()=>{
        console.log("*****")
        navigate(`/admin/my-profile/update`)
        
        }}>Update</button>
    </div>
  );
};

export default MyProfile;
