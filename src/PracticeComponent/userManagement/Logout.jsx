import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";

const Logout = () => {
  localStorage.removeItem("token");
  let navigate = useNavigate();

  useEffect(() => {
    navigate(`/admin/login`);
  }, []);

  return <div>Logout</div>;
};

export default Logout;
