import React, { useEffect, useState } from "react";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate, useParams } from "react-router-dom";

const UpdatePassword = () => {
  let token = localStorage.getItem(`token`);

  let [oldPassword, setOldPassword] = useState("");
  let [newPassword, setNewPassword] = useState("");

  let navigate = useNavigate();

  let onSubmit = async (e) => {
    e.preventDefault();

    let data = {
      oldPassword: oldPassword,
      newPassword: newPassword,
    };
    // console.log(data);
    try {
      let result = await axios({
        url: `http://localhost:8000/users/update-password`,
        method: "PATCH",
        data: data,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      localStorage.removeItem(`token`);
      navigate(`/admin/login`);
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };

  return (
    <div>
      <ToastContainer />
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="oldPassword">Old Password: </label>
          <input
            type="password"
            // placeholder="Eg: Gopal"
            value={oldPassword} //abc
            onChange={(e) => {
              setOldPassword(e.target.value);
            }}
            id="oldPassword"
          ></input>
        </div>
        <div>
          <label htmlFor="newPassword">New Password: </label>
          <input
            type="password"
            // placeholder="Eg: Gopal"
            value={newPassword} //abc
            onChange={(e) => {
              setNewPassword(e.target.value);
            }}
            id="newPassword"
          ></input>
        </div>
        <button
          type="submit"
          className="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
        >
          Update
        </button>
      </form>
    </div>
  );
};

export default UpdatePassword;
