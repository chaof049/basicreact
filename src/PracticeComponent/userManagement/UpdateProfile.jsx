import React, { useEffect, useState } from "react";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate, useParams } from "react-router-dom";

const UpdateProfile = () => {
  let token = localStorage.getItem(`token`);

  let [fullName, setFullName] = useState("");
  let [gender, setGender] = useState("");

  let navigate = useNavigate();

  let onSubmit = async (e) => {
    e.preventDefault();

    let data = {
      fullName: fullName,
      gender: gender,
    };
    // console.log(data);
    try {
      let result = await axios({
        url: `http://localhost:8000/users/myprofile`,
        method: "PATCH",
        data: data,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
    console.log("******")
      navigate(`/admin/my-profile`);
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };
  let getMyProfile = async () => {
    let result = await axios({
      url: `http://localhost:8000/users/myprofile`,
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    let data = result.data.result;
    setFullName(data.fullName);
    setGender(data.gender);
  };

  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];

  useEffect(() => {
    getMyProfile();
  }, []);
  return (
    <div>
      <ToastContainer />
      <form onSubmit={onSubmit}>
        <div>
          <div>
            <label htmlFor="fullName">Full Name : </label>
            <input
              type="text"
              id="fullName"
              value={fullName}
              onChange={(e) => {
                setFullName(e.target.value);
              }}
            ></input>
          </div>
          <br></br>
          <div>
            <label>Gender</label>

            {genders.map((item, i) => {
              return (
                <>
                  <label htmlFor={item.value}>{item.label}</label>
                  <input
                    type="radio"
                    value={item.value}
                    id={item.value}
                    checked={gender === item.value}
                    onChange={(e) => {
                      setGender(e.target.value);
                    }}
                  ></input>
                </>
              );
            })}
          </div>
          <br></br>
        </div>
        <button
          type="submit"
          className="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
        >
          Update
        </button>
      </form>
    </div>
  );
};

export default UpdateProfile;
