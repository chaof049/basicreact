import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Login = () => {
  let [email, setEmail] = useState();
  let [password, setPassword] = useState();

  let navigate = useNavigate();

  let onSubmit = async (e) => {
    e.preventDefault();

    let data = {
      email: email,
      password: password,
    };
    try {
      let result = await axios({
        url: "http://localhost:8000/users/login",
        method: "POST",
        data: data,
      });

      localStorage.setItem("token", result.data.token);
      navigate(`/admin/my-profile`);
    } catch (error) {
      toast.error(error.response.data.message);
    }
  };

  return (
    <div>
      <ToastContainer />
      <form onSubmit={onSubmit}>
        <label htmlFor="email">Email : </label>
        <input
          type="email"
          id="email"
          // placeholder="Eg: wangyi@yahoo.com"
          value={email}
          onChange={(e) => {
            setEmail(e.target.value);
          }}
        ></input>
        <br></br>
        <label htmlFor="password">Password : </label>
        <input
          type="password"
          id="password"
          value={password}
          onChange={(e) => {
            setPassword(e.target.value);
          }}
        ></input>
        <br></br>
        <button type="submit">Login</button>
      </form>
    </div>
  );
};

export default Login;
