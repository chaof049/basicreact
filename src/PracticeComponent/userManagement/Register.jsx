import axios from "axios";
import React, { useState } from "react";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Register = () => {
  let [fullName, setFullName] = useState("");
  let [email, setEmail] = useState("");
  let [password, setPassword] = useState("");
  let [dob, setDob] = useState("");
  let [gender, setGender] = useState("male");

  let onSubmit = async (e) => {
    e.preventDefault();
    let data = {
      fullName: fullName,
      email: email,
      password: password,
      dob: dob,
      gender: gender,
    };

    data = { ...data, role: "superadmin" };

    let result = await axios({
      url: "http://localhost:8000/users",
      method: "POST",
      data: data,
    });

    setFullName("");
    setEmail("");
    setPassword("");
    setDob("");
    setGender("");

    toast.success(
      "Mail has been sent to your email. Please verify your account by clicking the link on your mail"
    );
  };
  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ];
  return (
    <div>
      <ToastContainer />
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="fullName">Full Name : </label>
          <input
            type="text"
            id="fullName"
            // placeholder="Eg: wangyi"
            value={fullName}
            onChange={(e) => {
              setFullName(e.target.value);
            }}
          ></input>
          <br></br>
          <label htmlFor="email">Email : </label>
          <input
            type="email"
            id="email"
            // placeholder="Eg: wangyi@yahoo.com"
            value={email}
            onChange={(e) => {
              setEmail(e.target.value);
            }}
          ></input>
          <br></br>
          <label htmlFor="password">Password : </label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          ></input>
          <br></br>
          <label htmlFor="dob">Date Of Birth : </label>
          <input
            type="date"
            id="dob"
            value={dob}
            onChange={(e) => {
              setDob(e.target.value);
            }}
          ></input>
          <br></br>
        </div>
        <div>
          <label htmlFor="male">Gender : </label>

          {genders.map((item, i) => {
            return (
              <>
                <label htmlFor={item.value}>{item.label}</label>
                <input
                  type="radio"
                  value={item.value}
                  id={item.value}
                  checked={gender === item.value}
                  onChange={(e) => {
                    setGender(e.target.value);
                  }}
                ></input>
              </>
            );
          })}
        </div>
        <button
          type="submit"
          className="rounded-md bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600"
        >
          Register
        </button>
      </form>
    </div>
  );
};

export default Register;
