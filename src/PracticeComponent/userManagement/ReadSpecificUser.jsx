import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
const ReadSpecificUser = () => {
  let [user, setUser] = useState({});
  let params = useParams();
  let token = localStorage.getItem("token");
  let getUsers = async () => {
    try {
      let result = await axios({
        url: `http://localhost:8000/users/${params.id}`,
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      //   console.log(result);
      setUser(result.data.result);
    } catch (error) {
      console.log(error.response.data.message);
    }
  };
  useEffect(() => {
    getUsers();
  }, []);
  return (
    <div style={{ border: "2px solid black", margin: "20px 20px" }}>
      <p>Full Name: {user.fullName}</p>
      <p>Email: {user.email}</p>
      <p>DOB Name: {new Date(user.dob).toLocaleString()}</p>
      <p>Gender: {user.gender}</p>
      <p>Role: {user.role}</p>
    </div>
  );
};
export default ReadSpecificUser;
