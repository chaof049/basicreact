import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
const ReadUsers = () => {
  let navigate = useNavigate();
  const [users, setUsers] = useState([]);
  const token = localStorage.getItem("token");
  let getUsers = async () => {
    try {
      let result = await axios({
        url: "http://localhost:8000/users",
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      console.log(result.data.result);
      setUsers(result.data.result);
    } catch (error) {
      console.error("Error fetching users:", error);
    }
  };
  let handleSpecificUser = (id) => {
    return (e) => {
      navigate(`/admin/${id}`);
    };
  };
  useEffect(() => {
    getUsers();
  }, []);
  return (
    <div>
      <h2>User List</h2>
      {users.map((user, i) => (
        <div
          style={{
            border: "2px solid black",
            marginTop: "15px",
            marginLeft: "15px",
          }}
        >
          <p>User {i + 1}</p>
          <p>Full Name: {user.fullName}</p>
          <p>Email: {user.email}</p>
          <p>DOB Name: {new Date(user.dob).toLocaleString()}</p>
          <p>Gender: {user.gender}</p>
          <p>Role: {user.role}</p>
          <button onClick={handleSpecificUser(user._id)}>View me</button>
        </div>
      ))}
    </div>
  );
};
export default ReadUsers;
