import axios from "axios";
import React, { useState } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";

const ResetPassword = () => {
  let [password, setPassword] = useState("");

  let [query] = useSearchParams();
  let token = query.get("token");
  // console.log(token);

  let navigate = useNavigate();

  let onSubmit = async (e) => {
    e.preventDefault();

    let data = {
      password: password,
    };

    try {
      let result = await axios({
        url: "http://localhost:8000/users/reset-password",
        method: "PATCH",
        data: data,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      navigate(`/admin/login`);
    } catch (error) {
      console.log(error.message);
    }
  };

  return (
    <div>
      <form onSubmit={onSubmit}>
        <div>
          <label htmlFor="password">Password: </label>
          <input
            type="password"
            value={password} //abc
            onChange={(e) => {
              setPassword(e.target.value);
            }}
            id="password"
          ></input>
        </div>

        <button type="submit">Reset password</button>
      </form>
    </div>
  );
};

export default ResetPassword;
