import React, { useState } from "react";

const HideAndShowPassword = () => {
  let [password, setPassword] = useState();
  let [type, setType] = useState("password");
  return (
    <div>
      <form>
        <div>
          <label htmlFor="password">Password: </label>
          <input
            id="password"
            type={type}
            value={password}
            onChange={(e) => {
              setPassword(e.target.value);
            }}
          ></input>
        </div>
        <button
          type="button"
          onClick={() => {
            if (type === "password") return setType("text");
            else if (type === "text") return setType("password");
          }}
        >
          {type === "text" ? "Hide Password" : "Show Password"}
        </button>
      </form>
    </div>
  );
};

export default HideAndShowPassword;
