import React, { useEffect, useState } from 'react'

const LearnCleanUpFunction = () => {
  let [count, setCount]=useState(0)

    useEffect(()=>{
        console.log("i am useEffect function")
        return(()=>{
            console.log("i am clean up function")
        })
    },[count])
  return (
    <div>
        {count}
        <br></br>
        <button
        onClick={(e)=>{
            setCount(count+1)
        }}
        >Increment</button>
    </div>
  )
}

export default LearnCleanUpFunction