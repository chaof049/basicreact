import React, { useEffect, useState } from 'react'

const LearnUseEffect2 = () => {
  let [count, setCount]=useState(0)
    let [count2, setCount2]=useState(100)

    useEffect(()=>{
        console.log("i am useEffect function")
    },[count,count2])

    useEffect(()=>{
        console.log("i am useEffect2")
    },[])

    useEffect(()=>{
        console.log("i am useEffect2")
    })
    // for no dependency useEffect(function) => function will execute in each render
    // for empty dependency useEffect(function,[]) => function will execute at first render only
    // for [count,count2] dependency useEffect(function, [count,count2]) => function will execute if one of the dependency changes
  return (
    <div>
        {count}
        <br></br>
        <button
        onClick={()=>{
            setCount(count+1)
        }}
        >Increment</button>
        <br></br>
        {count2}
        <br></br>
        <button
        onClick={()=>{
            setCount2(count2+1)
        }}
        >Increment</button>
    </div>
  )
}

export default LearnUseEffect2