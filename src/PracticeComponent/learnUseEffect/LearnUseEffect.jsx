import React, { useEffect, useState } from 'react'

const LearnUseEffect = () => {
    let [count, setCount]=useState(0)
    let [count2, setCount2]=useState(100)

    useEffect(()=>{
        console.log("i am useEffect function")
    },[count===5,count2===102])
    console.log("i am component")
  return (
    <div>
        {count}
        <br></br>
        <button
        onClick={()=>{
            setCount(count+1)
        }}
        >Increment</button>
        <br></br>
        {count2}
        <br></br>
        <button
        onClick={()=>{
            setCount2(count2+1)
        }}
        >Increment</button>
    </div>
  )
}

export default LearnUseEffect