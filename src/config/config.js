import { config } from "dotenv";

config();

export let atlasEmail = process.env.ATLAS_EMAIL;
export let atlasPassword = process.env.ATLAS_PASSWORD;
export let atlasClient = process.env.ATLAS_CLIENT;
export let atlasDatabase = process.env.ATLAS_DATABASE;
export let atlasCollection = process.env.ATLAS_COLLECTION;
export let applicationId = process.env.APPLICATION_ID;
