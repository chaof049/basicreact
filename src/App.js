import { useState } from "react";
import "./App.css";
import Age from "./PracticeComponent/zclasspractice/Age";
import ButtonClick from "./PracticeComponent/zclasspractice/ButtonClick";
import Detail from "./PracticeComponent/zclasspractice/Detail";
import DifferentDataEffect from "./PracticeComponent/zclasspractice/DifferentDataEffect";
import Image from "./PracticeComponent/zclasspractice/Image";
import Info from "./PracticeComponent/zclasspractice/Info";
import LearnTearnary from "./PracticeComponent/zclasspractice/LearnTearnary";
import Location from "./PracticeComponent/zclasspractice/Location";
import MapPractice from "./PracticeComponent/zclasspractice/MapPractice";
import MapPractice2 from "./PracticeComponent/zclasspractice/MapPractice2";
import Name from "./PracticeComponent/zclasspractice/Name";
import LearnCleanUpFunction from "./PracticeComponent/learnUseEffect/LearnCleanUpFunction";
import LearnUseEffect from "./PracticeComponent/learnUseEffect/LearnUseEffect";
import LearnUseEffect2 from "./PracticeComponent/learnUseEffect/LearnUseEffect2";
import ImageUseState from "./PracticeComponent/learnUseState/ImageUseState";
import Increment from "./PracticeComponent/learnUseState/Increment";
import LearnUseState1 from "./PracticeComponent/learnUseState/LearnUseState1";
import ShowAndHideImage from "./PracticeComponent/learnUseState/ShowAndHideImage";
import Toggle from "./PracticeComponent/learnUseState/Toggle";
import WhyUseState from "./PracticeComponent/learnUseState/WhyUseState";
import MyRoutes from "./PracticeComponent/MyRoutes";
import NestingRoute from "./PracticeComponent/learnRouting/NestingRoute";
import ReactRoutes from "./PracticeComponent/learnRouting/ReactRoutes";
import Form from "./PracticeComponent/Form/Form";
import FormikForm from "./PracticeComponent/learnFormik/FormikForm";
import FormikTutorial from "./PracticeComponent/learnFormik/FormikTutorial";
import UploadImage from "./PracticeComponent/Upload/UploadImage";
import PracticeForm from "./PracticeComponent/practiceForm/PracticeForm";
import PracticeForm2 from "./PracticeComponent/practiceForm/PracticeForm2";
import UseRef1 from "./PracticeComponent/learnUseRef/useRef1";
import HideAndShowPassword from "./PracticeComponent/HideAndShowPassword";
import LearnLocalStorage from "./PracticeComponent/learnLocalStorage/LearnLocalStorage";
import GetLocalStorageItem from "./PracticeComponent/learnLocalStorage/GetLocalStorageItem";
import ClearLocalStorageItem from "./PracticeComponent/learnLocalStorage/ClearLocalStorageItem";
import LearnSessionStorage from "./PracticeComponent/learnSessionStorage/LearnSessionStorage";
import GetSessionStorage from "./PracticeComponent/learnSessionStorage/GetSessionStorage";
import CreateForm from "./Project/CreateForm";

function App() {
  // let tag1 = <div>first</div>;
  // let a = 1;
  // let b = 2;
  let [showComp, setShowComp] = useState(true);
  return (
    <div>
      {/* <div style={{ color: "pink", backgroundColor: "yellow" }}>
        this is sparta
      </div>
      <div className="success">class name dw9</div>
      <div className="failure">class name dw8</div>
      {tag1}
      {a + b} */}
      {/* <Name></Name>
      <Age></Age>
      <Detail name="chaofan" age={33}></Detail>
      <Location
        country="nepal"
        province={3}
        district="bhaktapur"
        exactLocation="sallaghari"
      ></Location>
      <Info
        name="chaofan"
        age={33}
        fatherDetails={[{ fname: "dawei" }, { fage: 44 }]}
        favFood={["burger", "niurou"]}
      ></Info>
      <Image></Image> */}
      {/* <LearnTearnary></LearnTearnary> */}
      {/* <DifferentDataEffect></DifferentDataEffect> */}
      {/* <MapPractice></MapPractice> */}
      {/* <MapPractice2></MapPractice2> */}
      {/* <ButtonClick></ButtonClick> */}
      {/* <LearnUseState1></LearnUseState1> */}
      {/* <ImageUseState></ImageUseState> */}
      {/* <ShowAndHideImage></ShowAndHideImage> */}
      {/* <Toggle></Toggle> */}
      {/* <WhyUseState></WhyUseState> */}
      {/* <Increment></Increment> */}
      {/* <LearnUseEffect></LearnUseEffect> */}
      {/* <LearnUseEffect2></LearnUseEffect2> */}
      {/* {showComp ? <LearnCleanUpFunction></LearnCleanUpFunction> : null}
      <button
        onClick={(e) => {
          setShowComp(true);
        }}
      >Show
      </button>
      <button
        onClick={(e) => {
          setShowComp(false);
        }}
      >Hide
      </button> */}
      {/* <MyRoutes></MyRoutes> */}
      {/* <NestingRoute></NestingRoute> */}
      {/* <ReactRoutes></ReactRoutes> */}
      {/* <Form></Form> */}
      {/* <FormikForm></FormikForm> */}
      {/* <FormikTutorial></FormikTutorial> */}
      {/* <UploadImage></UploadImage> */}
      {/* <PracticeForm></PracticeForm> */}
      {/* <PracticeForm2></PracticeForm2> */}
      {/* <UseRef1></UseRef1> */}
      {/* <HideAndShowPassword></HideAndShowPassword> */}
      {/* <LearnLocalStorage></LearnLocalStorage> */}
      {/* <GetLocalStorageItem></GetLocalStorageItem> */}
      {/* <ClearLocalStorageItem></ClearLocalStorageItem> */}
      {/* <LearnSessionStorage></LearnSessionStorage> */}
      {/* <GetSessionStorage></GetSessionStorage> */}
      <CreateForm></CreateForm>
    </div>
  );
}

export default App;
