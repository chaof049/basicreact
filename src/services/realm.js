import Realm from "realm";
import {
  applicationId,
  atlasClient,
  atlasCollection,
  atlasDatabase,
  atlasEmail,
  atlasPassword,
} from "../config/config.js";

const realmApp = new Realm.App({ id: applicationId });

const realmService = {
  init: () => {
    // Any initialization logic
  },

  // User authentication functions
  login: async () => {
    const email = atlasEmail;
    const password = atlasPassword;
    try {
      await realmApp.logIn(Realm.Credentials.emailPassword(email, password));
      console.log("Successfully logged in!");
    } catch (error) {
      console.error("Failed to log in:", error.message);
    }
  },
  register: async (email, password) => {
    // Registration logic
  },
  logout: async () => {
    // Logout logic
  },

  // Data operations
  fetchData: async () => {
    const mongodb = realmApp.currentUser.mongoClient(atlasClient);
    const collection = mongodb.db(atlasDatabase).collection(atlasCollection);
    const documents = await collection.find({});
    console.log("Documents:", documents);
  },
  createDocument: async (data) => {
    // Create document logic
  },
  updateDocument: async (id, data) => {
    // Update document logic
  },
  deleteDocument: async (id) => {
    // Delete document logic
  },
};

export default realmService;
