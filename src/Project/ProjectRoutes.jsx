import React from 'react'
import { Outlet, Route, Routes } from 'react-router-dom';
import ProjectLinks from './ProjectLinks';
import ReadAllForm from './ReadAllForm';
import CreateForm from './CreateForm';
import ReadSpecificForm from './ReadSpecificForm';

const ProjectRoutes = () => {
  return (
    <div>
      <Routes>
        <Route
          path="/"
          element={
            <div>
              <ProjectLinks></ProjectLinks>
              <Outlet></Outlet>
              {/* <div>This is footer</div> */}
            </div>
          }
        >
          <Route index element={<div> this is home page</div>}></Route>
          <Route
            path="forms"
            element={
              <div>
                <Outlet></Outlet>
              </div>
            }
          >
            <Route index element={<ReadAllForm></ReadAllForm>}></Route>
            <Route
              path=":id"
              element={<ReadSpecificForm></ReadSpecificForm>}
            ></Route>
            <Route path="create" element={<CreateForm></CreateForm>}></Route>
            {/* <Route
              path="update"
              element={
                <div>
                  <Outlet></Outlet>
                </div>
              }
            >
              <Route
                path=":id"
                element={<UpdateProduct></UpdateProduct>}
              ></Route>
            </Route> */}
          </Route>


        </Route>

        <Route path="*" element={<div>404 page</div>}></Route>
      </Routes>
    </div>
  );
}

export default ProjectRoutes