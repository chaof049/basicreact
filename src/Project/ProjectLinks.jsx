import React from "react";
import { NavLink } from "react-router-dom";

const ProjectLinks = () => {
  return (
    <div>
      <NavLink to="http://localhost:3000/forms" className="mr-2">
        Form
      </NavLink>
      <NavLink to="http://localhost:3000/forms/create" className="mr-2">
        Create Form
      </NavLink>
    </div>
  );
};

export default ProjectLinks;
