import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "react-toastify/dist/ReactToastify.css";

const ReadAllForm = () => {
  let [forms, setForms] = useState([]);
  let navigate = useNavigate();

  let getAllForms = async () => {
    try {
      let result = await axios({
        url: "http://localhost:8000/forms",
        method: "GET",
      });
      setForms(result.data.result);
    } catch (error) {
      console.log(error.response.data.message);
    }
  };
  useEffect(() => {
    getAllForms();
  }, []);
  return (
    <>
      {forms.map((item, i) => {
        return (
          <div key={i} class="border-dashed border-2 border-indigo-600 mb-2">
            <p>File name is {item.name}</p>
            <p>Image : {item.profileImage}</p>
            <button className="rounded bg-indigo-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-indigo-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 mr-4">
              Edit
            </button>
            <button
              className="rounded bg-green-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-green-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 mr-4"
              onClick={() => {
                navigate(`/forms/${item._id}`);
              }}
            >
              View
            </button>
            <button
              className="rounded bg-red-600 px-3 py-2 text-sm font-semibold text-white shadow-sm hover:bg-red-500 focus-visible:outline focus-visible:outline-2 focus-visible:outline-offset-2 focus-visible:outline-indigo-600 mr-4"
              onClick={async (e) => {
                try {
                  let result = await axios({
                    url: `http://localhost:8000/forms/${item._id}`,
                    method: "DELETE",
                  });
                } catch (error) {
                  console.log(error.response.data.message);
                }
              }}
            >
              Delete
            </button>
          </div>
        );
      })}
    </>
  );
};

export default ReadAllForm;
