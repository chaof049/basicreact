import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

const ReadSpecificForm = () => {
  let [form, setForm] = useState({});

  let params = useParams();

  let getForm = async () => {
    let result = await axios({
      url: `http://localhost:8000/forms/${params.id}`,
      method: "GET",
    });
    setForm(result.data.result);
  };
  useEffect(() => {
    getForm();
  }, []);
  return (
    <div>
      <p>name is {form.name}</p>
      <p>Image: {form.profileImage}</p>
    </div>
  );
};

export default ReadSpecificForm;
