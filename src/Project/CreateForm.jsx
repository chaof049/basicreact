import React from "react";
import axios from "axios";
import { Form, Formik } from "formik";
import FormikInput from "../PracticeComponent/learnFormik/FormikInput";
import FormikImage from "../PracticeComponent/learnFormik/FormikImage";

const CreateForm = () => {
  let initialValues = {
    name: "",
    profileImage: "",
  };
  let onSubmit = async (values, other) => {
    console.log(values);
    try {
      let result = await axios({
        url: `http://localhost:8000/forms`,
        method: "POST",
        data: values,
      });
      console.log(result);
    } catch (error) {
      console.log(error.response.data.message);
    }
  };

  return (
    <div>
      <Formik initialValues={initialValues} onSubmit={onSubmit}>
        {({}) => {
          return (
            <Form>
              <FormikInput
                name="name"
                label="Name: "
                type="text"
                required={true}
              ></FormikInput>
              <FormikImage
                name="profileImage"
                label="Profile Image"
              ></FormikImage>
              <button type="submit">Submit</button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

export default CreateForm;
